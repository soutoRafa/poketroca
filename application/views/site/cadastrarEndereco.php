<div id="modalcadastro" class="modal fade" role="dialog">

<div class="modal-dialog">
   
    <div class="modal-content">
   
      <div class="modal-header">
      	<div class="page-header">
       		<button type="button" class="close" data-dismiss="modal">&times;</button>  
	  		<h4 class="modal-title">Cadastre seu endereço para continuar!</h4>
		</div>
		<div class="container">
			<div class="col-md-10">
				<div class="col-md-6">	
	        		
					<?php 
						echo validation_errors('<div class="alert alert-danger">', '</div>');
						echo form_open('site/usuario/cadastraEndereco');
					?>
					<label>Estado</label>	
					<div class="form-group">
						<input type="text" name="estado" class="form-control" placeholder="UF" maxlength="2">
					</div>
					
					<label>Cidade</label>	
					<div class="form-group">
						<input type="text" name="cidade" class="form-control" placeholder="Cidade">
					</div>

					<label>CEP</label>	
					<div class="form-group">
						<input type="text" name="cep" class="form-control" placeholder="CEP" maxlength="8">
					</div>

					<label>Endereço</label>	
					<div class="form-group">
						<input type="text" name="endereco" class="form-control" placeholder="Rua, bairro">
					</div>
					
					<label>Complemento</label>	
					<div class="form-group">
						<input type="text" name="complemento" class="form-control" placeholder="bloco, número">
					</div>
					
					<label>Telefone</label>	
					<div class="form-group">
						<input type="text" name="telefone" class="form-control" placeholder="Telefone residencial">
					</div>

					<label>Celular</label>	
					<div class="form-group">
						<input type="text" name="celular" class="form-control" placeholder="Telefone móvel">
					</div>
					
					<button class="btn btn-primary" name="cadastrar" type="submit">Cadastrar</button>
					
					<?php echo form_close(); ?>
			</div>
		</div>
	</div>	<!-- col-md-12 -->	
</div>	<!-- container -->
           
</div> <!-- FIM DO CORPO DO MODAL -->
</div> <!-- FIM DO CONTEUDO MODAL -->
</div> <!-- ROW -->
</div>  <!-- col-md8 -->
</div> <!-- FIM DA DIALOG -->
</div> <!-- FECHA O MODAL -->
