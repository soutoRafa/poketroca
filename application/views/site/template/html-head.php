<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PokéTrade</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- CSS GERAL -->
    <link href="<?php echo base_url('assets/css/site.css');?>" rel="stylesheet">
    <!-- CSS página inicial-->
    <link href="<?php echo base_url('assets/css/usuario.css');?>" rel="stylesheet">
    <!-- CSS endereco -->
    <link href="<?php echo base_url('assets/css/usuarioperfil.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/fonte/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body>    