<footer class="navbar-static-bottom">
	<div class="container">
		
		<div class="col-md-4">
			<label>Redes sociais:</label>
			<div class="social">
				<a href="https://www.facebook.com/souto.rafael">
					<i class="fa fa-facebook-square fa-3x fb" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="social">
				<a href="https://twitter.com/ObliviusOfJoy">
					<i class="fa fa-twitter-square fa-3x fb" aria-hidden="true"></i>
				</a>
			</div>
			<div class="social">
				<a href="https://www.instagram.com/suchrafez/">
					<i class="fa fa-instagram fa-3x fb" aria-hidden="true"></i>
				</a>
				
			</div>
		</div>	
	</div>
</footer>	    