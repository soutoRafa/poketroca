<div class="page-header">
    <nav class="navbar navbar-static-top navbar-default nav-header">
        <div class="container">
            <div class="row">       
                <div class="navbar-header">
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" 
                data-target="#menu-navegacao">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <a href="<?php echo base_url('usuario'); ?>" class="navbar-brand">
                        <div id="logo"></div>
                    </a>
                </div><!-- HEADER -->
                
                <div class="collapse navbar-collapse" id="menu-navegacao">
                   
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url('usuario') ?>">Página inicial</a></li>
                        <li><a href="<?php echo base_url('usuario/perfil') ?>"> Perfil </a></li>
                        <li><a href=""> Minhas Trocas </a></li>
                        <li><a href=""> Minhas Cartas  </a></li>
                        <li><a href=""> Sair </a></li>
                    </ul>
                
                </div>
            </div><!-- ROW -->
        </div><!-- CONTAINER -->
    </nav> <!-- NAVBAR -->        
</div>