<section id="perfil-usuario">
	
	<ol class="breadcrumb">
 		<li><a href="<?php echo base_url('site/usuario'); ?>">Home</a></li>
 		<li>Perfil</li>
	</ol>
	
	<div id="botao-edicao">
		<button class="btn btn-default btn-lg edicao" id="btndados">
			<i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
			Editar dados de usuário
		</button>
		<button class="btn btn-default btn-lg edicao" id="btnend">
			<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
			Editar dados de endereço
		</button>	
	</div>
	
	<div id="formulario-usuario">

	<div class="page-header"><h4>Atualizar dados de usuário</h4> </div>
		<?php 
			echo validation_errors('<div class="alert alert-danger">', '</div>'); 
			echo form_open(base_url('site/usuario/atualizaDados'));
			
			foreach ($usuario as $user) { 
		?>
		
		<div class="col-md-12">
			<label for="nome">Nome</label>
			<div class="form-group">
				<input type="text" name="nome" class="form-control" id="nome" value="<?php echo $user->nome ?>">	
			</div>			
			<label id="email">Email</label>
			<div class="form-group">
				<input type="text" name="email" class="form-control" id="email" value="<?php echo $user->email ?>">	
			</div>
			
			<label for="senha">Senha</label>
			<div class="form-group">
				<input type="password" name="senha" class="form-control" id="senha">	
			</div>

			<label id="confirma">Confirma senha</label>
			<div class="form-group">
				<input type="password" name="confirma-senha" class="form-control" id="confirma">	
			</div>
			
			<button type="submit" class="btn btn-primary">Atualizar</button>	
		
		</div>
		
		<?php 
		} 
			echo form_close(); 
		?>	
	</div>
	
	<div id="formulario-endereco">
		<div class="page-header">Atualizar endereço</div>
		<?php 
			echo validation_errors('<div class="alert alert-danger">', '</div>'); 
			echo form_open(base_url('site/usuario/atualizaEndereco'));

			foreach ($endereco as $end) {
		
			
		?>		
		<div class="col-md-12">
			
			<label for="estado">Estado</label>
			<div class="form-group">
				<input type="text" name="estado" id="estado" class="form-control" value="<?php echo $end->estado ?>">	
			</div>
			
			<label for="cidade">Cidade</label>
			<div class="form-group">
				<input type="text" name="cidade" id="cidade" class="form-control" value="<?php echo $end->cidade ?>">	
			</div>

			<label for="cep">CEP</label>
			<div class="form-group">
				<input type="text" name="cep" id="cep" class="form-control" value="<?php echo $end->cep ?>">	
			</div>

			<label for="end">Endereço</label>
			<div class="form-group">
				<input type="text" name="endereco" id="end" class="form-control" value="<?php echo $end->endereco ?>">	
			</div>

			<label for="complemento">Complemento</label>
			<div class="form-group">
				<input type="text" name="complemento" id="complemento" class="form-control" value="<?php echo $end->complemento ?>">	
			</div>

			<label for="telefone">Telefone</label>
			<div class="form-group">
				<input type="text" name="telefone" id="telefone" class="form-control" value="<?php echo $end->telefone ?>">	
			</div>

			<label for="celular">Celular</label>
			<div class="form-group">
				<input type="text" name="celular" id="celular" class="form-control" value="<?php echo $end->celular ?>">	
			</div>

			<button type="submit" class="btn btn-primary">Atualizar</button>
		</div>
		<?php 
			}
			echo form_close();
		?>	
	</div>
	<div id="dados-usuario">
		
		<h4>Dados pessoais</h4>
		
		<?php foreach ($usuario as $user) { ?>
			<div id="imagem-perfil">
				<img class="img-responsive" src="<?php echo base_url('./assets/imagens/tyrani.png'); ?>"><br>
			</div>
			<label>Nome: <?php echo $user->nome ?></label><br>
			<label>Email: <?php echo $user->email ?></label><br>		
		<?php } ?>
		<hr>
		<h4>Endereço Cadastrado</h4>
		
		<?php foreach ($endereco as $end) { ?>
			<label>Estado: <?php echo $end->estado ?></label><br>
			<label>Cidade: <?php echo $end->cidade ?></label><br>
			<label>CEP: <?php echo $end->cep ?></label><br>
			<label>Endereço: <?php echo $end->endereco ?></label><br>
			<label>Complemento: <?php echo $end->complemento ?></label><br>
			<label>Telefone: <?php echo $end->telefone ?></label><br>
			<label>Celular: <?php echo $end->celular ?></label><br>

		<?php } ?>
	</div>
</section>

<br>