<?php 
if ($endereco_flag == 0) { 
    require_once 'cadastrarEndereco.php';

} 
?>
<section id="conteudo">
<div class="page-header">

    <?php foreach ($usuario as $user) { ?>
     <h3><?php echo $mensagem.' '.$user->nome ?></h3>   
</div>
<div class="container">
    <div class="col-md-12">
        <div class="col-md-4">
            <div class="box-conteudo">
               
                <div class="page-header">
                    <h3>Perfil de usuário</h3>
                </div>   
                
                <div class="cont">
                    <?php echo img('assets/usuarios/'.$user->img); ?>
                    <div class="middle">
                        <div class="text">
                            <button class="botao-invisivel" data-toggle="modal" data-target="#fotoModal">
                                <i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i>
                            Editar foto
                            </button>
                        </div>
                    </div> 
                </div>
                
                <div id="fotoModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Alterar foto</h3>
                            </div>
                            <div class="modal-body">
                                <div class="cont-align">
                                    <div id="image-preview">
                                    <?php 
                                        echo form_open_multipart('site/usuario/atualizaFoto');
                                    ?>
                                        <label for="image-upload" id="image-label">Escolher foto</label>
                                        <input type="file" name="userfile" id="image-upload">
                                    </div>
                                </div>                                    
                                
                                <button type="submit" class="btn btn-primary">Atualizar</button>
                                
                                <?php echo form_close(); ?>    
                                                               
                            </div> <!-- CORPO MODAL -->
                        </div> <!-- CONTEUDO MODAL -->
                    </div> <!-- FIM DA ABA MODAL -->
                </div>  <!-- FIM DA MODAL -->      

                <div class="infos-perfil">
                    <p><?php echo $user->nome ?></p>
                </div>

                <div class="infos-perfil">
                    <p><?php echo $user->email ?></p>
                </div>
                
                <div class="infos-perfil">
                <p>Reputação:</p>
                    <div id="reputacao">
                    <?php for ($i=0; $i < $user->reputacao; $i++) { ?> 
                        <div class="userball"></div>       
                    <?php } ?>
                    </div>
                </div>
                
                <div class="clear"></div>
                <?php foreach ($endereco as $end) { ?>
                
                <div class="infos-perfil">
                    <p><?php echo $end->estado ?></p>
                </div>
                
                <div class="infos-perfil">
                    <p><?php echo $end->cidade ?></p>
                </div>
                
                <?php } ?>
                
                <div class="infos-perfil">
                    <button type="button" class="btn btn-link"><a  href="<?php echo base_url('usuario/perfil');?>">Editar perfil</a></button>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-4">
            <div class="box-conteudo">
                <div class="page-header">
                    <h3>Trocas pendentes</h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box-conteudo">
                <div class="page-header">
                <h3>Trocas realizadas</h3>
                </div>
            </div>
        </div>                
    </div> 
</div>
<br><br><br>

</section>