<section id="cont">
        <div id="logo"></div>
        <span id="logo-label">PokéTrade</span>
        <div style="clear: both;"></div>
        <div class="row">
            <div class="container">
                
                <div class="col-md-8">
                    <h1>Comece a trocar agora!</h1>
                    <p>PokéTrade é um sistema criado para a interação de colecionadores de cartas Pokémon TCG (trading card game) para que possam achar as cartas que procuram mais facilmente e adquiri-las de outros colecionadores.</p>
                    <p><a data-toggle="modal" data-target="#myModal" >Cadastre-se</a> no site, ache seus cards e comece a trocar!</p>
                </div>
                <div class="col-md-4">
                    <div class="box-login">
                    <?php 
                        if ($flag == 1) {
                         echo '<div class="alert alert-success"> Cadastro realizado com sucesso!</div>';
                        }
                        
                        echo  validation_errors('<div class="alert alert-danger">', '</div>');
                        echo form_open('home/login'); 
                    ?>
                
                        <div class="page-header">
                            <h3>Acesse sua conta PokéTrade</h3>
                        </div>
                       
                        <label for="nome">Email</label><br />
                        <div class="col-md-10 form-group" >
                            <input type="text" name="email" id="nome" class="form-control input">
                        </div>

                        <label for="pass">Senha</label><br />
                        <div class="col-md-10 form-group">
                            <input type="password" name="senha" id="pass" class="form-control input">
                        </div>

                        <button type="submit" name="btnlog" class="btn btn-primary">Entrar</button>
                    <?php echo form_close(); ?>
                    <h5>Não possui uma conta? <a data-toggle="modal" data-target="#myModal" >Cadastre-se aqui</a>
                    </div>
                </div>
            </div>  
        </div>  
    </section>
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
     
    <!-- Modal content-->
    <div class="col-md-10">  
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cadastre-se gratuitamente!</h4>
        
      </div>
        <div class="row">
            <div class="col-md-12">
            <div class="modal-body">
        
            <?php 
               
                echo validation_errors('<div class="alert alert-danger">', '</div>');
                echo form_open('home/cadastro');
            ?>
            
                <div class="form-group">
                    <label for="nomecad">Nome</label>
                    <input type="text" id="nomecad" name="nomecad" class="form-control">
                </div>
                
                <div class="form-group">
                    <label for="emailcad">Email</label>
                    <input type="text" id="emailcad" name="emailcad" class="form-control">
                </div>
                
                <div class="form-group">
                    <label for="senhacad">Senha</label>    
                    <input type="password" id="senhacad" name="senhacad" class="form-control">
                </div>

                <label>Sexo</label>
                <div class="radio">
                   <label> <input type="radio" name="sexo" value="M" checked> Masculino </label>
                </div>
                <div class="radio">
                   <label> <input type="radio" name="sexo" value="F"> Feminino </label>
                </div>
             <div class="form-group">
                <button type="submit" class="btn btn-primary" id="btn-center">Cadastrar</button>
            </div>
           
            <?php echo form_close(); ?>
            
           
</div> <!-- FIM DO CORPO DO MODAL -->
</div> <!-- FIM DO CONTEUDO MODAL -->
</div> <!-- ROW -->
</div>  <!-- col-md8 -->
</div> <!-- FIM DA DIALOG -->
</div> <!-- FECHA O MODAL -->
