<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Usuario_model extends CI_Model {
		
		public function usuarioLogado()
		{
			$id = $this->session->id;
			$this->db->select('nome, email, senha, reputacao, img');
			$this->db->where('id', $id);
			return $this->db->get('usuarios')->result();
		}
		public function verificaEndereco()
		{
			$id = $this->session->id;
			$this->db->from('enderecos');
			$this->db->where('usuario', $id);
			
			return $this->db->count_all_results();
		}
		public function cadastraEndereco($dados)
		{
			$dados['usuario'] = $this->session->id;
			return $this->db->insert('enderecos', $dados);
		}
		public function listarEndereco()
		{
			$id = $this->session->id;
			$this->db->where('usuario', $id);
			return $this->db->get('enderecos')->result();
		}
		public function atualizaDados($dados)
		{			
			$id = $this->session->id;
			$this->db->where('id', $id);
			return $this->db->update('usuarios', $dados);
		}
		public function atualizaEndereco($dados)
		{			
			$id = $this->session->id;
			$this->db->where('usuario', $id);
			return $this->db->update('enderecos', $dados);
		}
		public function adicionarFoto($nome)
		{
			$id = $this->session->id;
			$dados['img'] = $nome;
			$this->db->where('id', $id);
			return $this->db->update('usuarios', $dados);
		}
	
	}
	
	/* End of file Usuario_model.php */
	/* Location: ./application/models/Usuario_model.php */