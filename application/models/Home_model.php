<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Home_model extends CI_Model {
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function verificaLogin($email, $senha)
		{
			$this->db->select('id');
			$this->db->from('usuarios');
			$this->db->where('senha', md5($senha));
			$this->db->where('email', $email);
			
			return $this->db->get()->result();
		}
		public function cadastraUsuario($nome, $email, $senha, $sexo)
		{
			$dados['nome'] = $nome;
			$dados['senha'] = $senha;
			$dados['email'] = $email;
			$dados['sexo'] = $sexo;
			$dados['img'] = 'img-default.png';
			return $this->db->insert('usuarios', $dados);
		}
	
	}