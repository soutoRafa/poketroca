<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Home extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('Home_model', 'home');
		}
		public function index()
		{
			$dados['flag'] = 0;
			$this->load->view('html-head');
			$this->load->view('home', $dados);
			$this->load->view('html-footer');
		}

		public function login(){

			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('senha','senha','required|min_length[6]');
			
			if ($this->form_validation->run() == false) {
				$this->index();
			}else{
				$email = $this->input->post('email');
				$senha = $this->input->post('senha');
				$usuario = $this->home->verificaLogin($email, $senha);
				 
				if(count($usuario) == 1){
					$dados = array('id' => $usuario[0]->id, 'logado' => TRUE );
					$this->session->set_userdata($dados);

					redirect(base_url('usuario'));
				}else{
					$dados = NULL;
					$dados['logado'] = FALSE;
					$this->session->set_userdata($sessao);
					redirect(base_url('home'));
				}
			}
		}

		public function cadastro()
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nomecad', 'nome', 'required');
			$this->form_validation->set_rules('emailcad', 'email', 'required|valid_email|is_unique[usuarios.email]');
			$this->form_validation->set_rules('senhacad', 'senha', 'required|min_length[6]');
			$this->form_validation->set_rules('sexo', 'sexo', 'required');

			if ($this->form_validation->run() == FALSE) {
				$this->index();
			}else{
				$nome = $this->input->post('nomecad');
				$email = $this->input->post('emailcad');
				$senha = $this->input->post('senhacad');
				$sexo = $this->input->post('sexo');

				if ($this->home->cadastraUsuario($nome, $email, md5($senha), $sexo)) {
					$dados['flag'] = 1;
					$this->load->view('html-head');
					$this->load->view('home', $dados);
					$this->load->view('html-footer');
				}
			}

		}
	
	}
	
	