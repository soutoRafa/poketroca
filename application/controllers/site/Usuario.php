<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Usuario extends CI_Controller {
		public function __construct()
		{
			parent::__construct();
			$this->load->model('usuario_model', 'user');
			$this->usuario = $this->user->usuarioLogado();
			$this->load->library('form_validation');

		}

		public function index()
		{
			if(!$this->session->userdata('logado')){
				redirect(base_url('admin/login'));
			}

			$this->endereco = $this->user->listarEndereco();
			$this->endereco_flag = $this->user->verificaEndereco();
		
			$dados['usuario'] = $this->usuario;			
			$dados['mensagem'] = 'Seja bem vindo, ';
			$dados['endereco_flag'] = $this->endereco_flag;
			$dados['endereco'] = $this->endereco;
		
			$this->load->view('site/template/html-head', $dados);
			$this->load->view('site/template/header');
			$this->load->view('site/usuario');
			$this->load->view('site/template/footer');
			$this->load->view('html-footer');
		}

		public function perfil()
		{
			if(!$this->session->userdata('logado')){
				redirect(base_url('admin/login'));
			}

			$this->endereco = $this->user->listarEndereco();
			$dados['endereco'] = $this->endereco;
			$dados['usuario'] = $this->usuario;
			$this->load->view('site/template/html-head', $dados);
			$this->load->view('site/template/header');
			$this->load->view('site/perfil');
			$this->load->view('site/template/footer');
			$this->load->view('html-footer');
		}

		public function cadastraEndereco()
		{
			if(!$this->session->userdata('logado')){
				redirect(base_url('admin/login'));
			}

			$this->form_validation->set_rules('estado', 'Estado', 'required|max_length[2]');
			$this->form_validation->set_rules('cidade', 'Cidade', 'required');
			$this->form_validation->set_rules('cep', 'CEP', 'required|min_length[8]|numeric');
			$this->form_validation->set_rules('endereco', 'Endereço', 'required');
			$this->form_validation->set_rules('complemento', 'Complemento', 'required');
			$this->form_validation->set_rules('telefone', 'Telefone', 'max_length[10]');
			$this->form_validation->set_rules('celular', 'Celular', 'max_length[11]');
			
			if ($this->form_validation->run() == FALSE) {
				$this->index();	
			}else{
				$cidade = $this->input->post('cidade');
				$estado = $this->input->post('estado');
				$cep = $this->input->post('cep');
				$endereco = $this->input->post('endereco');
				$complemento = $this->input->post('complemento');
				$telefone = $this->input->post('telefone');
				$celular = $this->input->post('celular');
				$dados = array ('cidade' => $cidade,'estado' => $estado,'cep' => $cep,'endereco' => $endereco,'complemento' => $complemento,'telefone' => $telefone,'celular' => $celular);

				if ($this->user->cadastraEndereco($dados)) {
					redirect(base_url('site/usuario'));
				}else{
					echo 'Erro no sistema, tente novamente!';
				}
			}
		}

		public function atualizaDados()
		{
			if(!$this->session->userdata('logado')){
				redirect(base_url('admin/login'));
			}

			$this->form_validation->set_rules('nome', 'Nome', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[6]');
			$this->form_validation->set_rules('confirma-senha', 'Confirma senha', 'required|min_length[6]|matches[senha]');
			
			if ($this->form_validation->run() == FALSE) {
				$this->perfil();
			}else{
				$nome = $this->input->post('nome');
				$email = $this->input->post('email');	
				$senha = $this->input->post('senha');

				$dados = array('nome' => $nome, 'email' => $email, 'senha' => md5($senha));
				if ($this->user->atualizaDados($dados)) {
					redirect(base_url('site/usuario/perfil'));
				}else{
					echo 'Erro no sistema, tente novamente';
				}
			}			
		}

		public function atualizaEndereco()
		{
			if(!$this->session->userdata('logado')){
				redirect(base_url('admin/login'));
			}

			$this->form_validation->set_rules('estado', 'Estado', 'required|max_length[2]');
			$this->form_validation->set_rules('cidade', 'Cidade', 'required');
			$this->form_validation->set_rules('cep', 'CEP', 'required|min_length[8]|numeric');
			$this->form_validation->set_rules('endereco', 'Endereço', 'required');
			$this->form_validation->set_rules('complemento', 'Complemento');
			$this->form_validation->set_rules('telefone', 'Telefone', 'required|min_length[8]|numeric');
			$this->form_validation->set_rules('celular', 'Celular', 'required|min_length[9]|numeric');
			
			if ($this->form_validation->run() == FALSE) {
				
				$this->perfil();
			}else{
				
				$estado = $this->input->post('estado');
				$cidade = $this->input->post('cidade');
				$cep = $this->input->post('cep');
				$endereco = $this->input->post('endereco');
				$complemento = $this->input->post('complemento');
				$telefone = $this->input->post('telefone');
				$celular = $this->input->post('celular');
				
				$dados = array ('cidade' => $cidade,'estado' => $estado,'cep' => $cep,'endereco' => $endereco,'complemento' => $complemento,'telefone' => $telefone,'celular' => $celular);
				
					if ($this->user->atualizaEndereco($dados)) {
						
						redirect(base_url('site/usuario/perfil'));
					}else{
						
						echo 'Erro no sistema, tente novamente!';
					}
			}
		
		}

		public function atualizaFoto(){

		if(!$this->session->userdata('logado')){
			redirect(base_url('site/usuario/login'));
		}

	
		$config['upload_path'] = './assets/usuarios';
		$config['allowed_types'] = 'jpg|png|gif|jpeg';
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload()) {
			echo $this->upload->display_errors();
		}
		else
		{
			$data['dadosArquivo'] = $this->upload->data();	
			$config2['source_image'] = './assets/usuarios/'.$data['dadosArquivo']['file_name'];
			$config2['create_thumb'] = false;
			$config2['width'] = 200;
			$config2['height'] = 300;

			$this->load->library('image_lib', $config2);
			$nome = $data['dadosArquivo']['file_name'];

			if ($this->image_lib->resize()) {
				if($this->user->adicionarFoto($nome)){
					redirect(base_url('site/usuario/perfil'));
				}else
				{
					echo 'Houve um erro no sistema!';
				}

			}else{
				$this->image_lib->display_errors();
			}	
	
		}
	}
}	